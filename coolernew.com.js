(function () {
  function parseToInt(e) {
    if (!e) {
      return;
    }

    e = e
      .toString()
      .replace(/۰/g, "0")
      .replace(/۱/g, "1")
      .replace(/۲/g, "2")
      .replace(/۳/g, "3")
      .replace(/۴/g, "4")
      .replace(/۵/g, "5")
      .replace(/۶/g, "6")
      .replace(/۷/g, "7")
      .replace(/۸/g, "8")
      .replace(/۹/g, "9")
      .replace(/\D/g, "");
    e = parseInt(e, 10);
    return e;
  }

  function parseToFloat(e) {
    e2 = parseToInt(e2);
    if (0 <= e2 && e2 <= 5) return e2;
    if (6 <= e2 && e2 <= 10) return e2 / 2;
    if (11 <= e2 && e2 <= 50) return e2 / 10;
    if (51 <= e2 && e2 <= 99) return e2 / 20;
    if (e2 >= 100) return e2 / 100;
    return e2;
  }

  function parstToTimeStamp(num, type) {
    switch (type) {
      case "s":
        return parseToInt(num);
      case "m":
        return parseToInt(num) * 60;
      case "h":
        return parseToInt(num) * 3600;
      case "d":
        return parseToInt(num) * 86400;
      default:
        return parseToInt(num);
    }
  }

  function productPage() {
    var pageType = document.querySelector("body");
    pageType = pageType.className.match("single-product");
    if (pageType) return true;
    return false;
  }

  function extractProductData() {
    if (!productPage()) {
      return;
    }

    var productInfo = {};

    var json = null;
    try {
      var ldJsons = document.querySelectorAll(
        'script[type="application/ld+json"]'
      );
      ldJsons.forEach(function (item) {
        item = JSON.parse(item.innerText);
        item = item["@graph"];
        if (item) {
          json = item;
        }
      });
    } catch (error) {
      console.error(error);
    }

    // sku
    var sku = document.querySelector('input[name="product_id"]');
    var backupSku = document.querySelector('button[name="add-to-cart"]');
    sku = (sku && sku.value) || (backupSku && backupSku.value);
    if (sku) productInfo.sku = sku;
    else productInfo.sku = null;

    //title
    var title = document.querySelector("title");
    title = title && title.text.replace("- کولرنیو", "");
    var backupTitle = document.querySelector(".product_title h1");
    title = title || (backupTitle && backupTitle.firstChild.data);
    if (title) productInfo.title = title;
    else productInfo.title = null;

    // image
    var backupImage = null;
    var image = document.querySelector('meta[property="og:image"]');
    if (json[2]["@type"] == "ImageObject") backupImage = json[2].url;
    image = (image && image.content) || backupImage;
    if (image) productInfo.image = image;
    else productInfo.image = null;

    // price
    var price = document.querySelector("ins .woocommerce-Price-amount");
    var backupPrice = document.querySelector('meta[name="price"]');
    price =
      (price && parseToInt(price.innerText)) ||
      (backupPrice && parseToInt(backupPrice.content));
    if (price) productInfo.price = price;
    else productInfo.price = null;

    // isAvailable
    var isAvailable = document.querySelector('meta[name="availability"]');
    if (isAvailable && isAvailable.content == "true")
      productInfo.isAvailable = true;
    else productInfo.isAvailable = false;

    // discount
    var discount = null;
    if (
      document.querySelectorAll(
        'p del strong[class="nk_discount_label"] span'
      ) &&
      document.querySelectorAll(
        'p del strong[class="nk_discount_label"] span'
      )[1]
    ) {
      discount = document.querySelectorAll(
        'p del strong[class="nk_discount_label"] span'
      )[1].innerHTML;
    }
    if (discount) productInfo.discount = parseToFloat(discount);
    else productInfo.discount = 0;

    // expiration
    // i'm not sure you ment this kind of expiration :)... but for the sake fo challange...
    var expiration = document.querySelectorAll(".timer_nums .num");
    if (expiration[0]) {
      var s = parstToTimeStamp(expiration[0].innerHTML, "s");
      var m = parstToTimeStamp(expiration[1].innerHTML, "m");
      var h = parstToTimeStamp(expiration[2].innerHTML, "h");
      var d = parstToTimeStamp(expiration[3].innerHTML, "d");
      var currentTime = Math.floor(Date.now() / 1000);
      expiration = currentTime + s + m + h + d;
    } else expiration = 0;
    if (expiration) productInfo.expiration = expiration;
    else productInfo.expiration = null;

    // category
    var category = [];
    var tmpCategory = document.querySelectorAll(".woocommerce-breadcrumb li a");
    for (var i = 2; i < tmpCategory.length; i++) {
      category.push(tmpCategory[i].innerHTML);
    }
    if (category) productInfo.category = category;

    // brand
    var brand = document.querySelector(".brand_title a");
    if (brand) productInfo.brand = brand.innerHTML;
    else productInfo.brand = null;

    // totalVotes
    var totalVotes = document.querySelector(".woocommerce-review-link");
    if (totalVotes) productInfo.totalVotes = parseToInt(totalVotes.innerHTML);
    else productInfo.totalVotes = null;

    // averageVote
    var averageVote = document.querySelector('strong[class="rating"]');
    if (averageVote && totalVotes)
      productInfo.averageVote = parseToFloat(averageVote.innerHTML);
    else productInfo.averageVote = null;

    return productInfo;
  }

  var sentFlag = false;
  function sendData() {
    try {
      var productInfo = extractProductData();

      if (productInfo && productInfo.sku && productInfo.title) {
        console.log(productInfo);
        sentFlag = true;
        // eslint-disable-next-line no-undef
        // yektanet.product('detail', productInfo);
      }
    } catch (e) {
      console.error(e);
      return false;
    }
  }

  var retry = 0;
  var intervalId = setInterval(function () {
    sendData();
    if (sentFlag == true || retry > 3) {
      clearInterval(intervalId);
    }
    retry++;
  }, 2000);
})();
