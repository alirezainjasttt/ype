(function () {
  function parseToInt(e) {
    if (!e) {
      return;
    }

    e = e
      .toString()
      .replace(/۰/g, "0")
      .replace(/۱/g, "1")
      .replace(/۲/g, "2")
      .replace(/۳/g, "3")
      .replace(/۴/g, "4")
      .replace(/۵/g, "5")
      .replace(/۶/g, "6")
      .replace(/۷/g, "7")
      .replace(/۸/g, "8")
      .replace(/۹/g, "9")
      .replace(/\D/g, "");
    e = parseInt(e, 10);
    return e;
  }

  function parseToFloat(e) {
    e2 = parseToInt(e2);
    if (0 <= e2 && e2 <= 5) return e2;
    if (6 <= e2 && e2 <= 10) return e2 / 2;
    if (11 <= e2 && e2 <= 50) return e2 / 10;
    if (51 <= e2 && e2 <= 99) return e2 / 20;
    if (e2 >= 100) return e2 / 100;
    return e2;
  }

  function parsToTimeStamp(num, type) {
    switch (type) {
      case "s":
        return parseToInt(num);
      case "m":
        return parseToInt(num) * 60;
      case "h":
        return parseToInt(num) * 3600;
      case "d":
        return parseToInt(num) * 86400;
      default:
        return parseToInt(num);
    }
  }

  function productPage() {
    var pageType = document.querySelector('meta[property="og:type"]');
    if (pageType.content == "product") return true;
    return false;
  }

  function extractProductData() {
    if (!productPage()) {
      return;
    }

    var productInfo = {};

    var json = null;
    try {
      var ldJsons = document.querySelectorAll(
        'script[type="application/ld+json"]'
      );
      ldJsons.forEach(function (item) {
        item = JSON.parse(item.innerText);
        if (item["@type"] == "Product") {
          json = item;
        }
      });
    } catch (error) {
      console.error(error);
    }

    // sku
    var sku = document.querySelector('input[name="product_id"]');
    var backupSku = document.querySelector('input[name="add-to-cart"]');
    sku = (sku && parseToInt(sku.value)) || (backupSku && backupSku.value);
    if (sku) productInfo.sku = sku;
    else productInfo.sku = null;

    //title
    var title = document.querySelector(".product_title h1");
    var backupTitle = json && json.name;
    title = (title && title.innerText) || backupTitle;
    if (title) productInfo.title = title;
    else productInfo.title = null;

    // image
    var backupImage = null;
    var image = document.querySelector('meta[property="og:image"]');
    if (json.image["@type"] == "ImageObject") backupImage = json.image.url;
    image = image.content || backupImage;
    if (image) productInfo.image = image;
    else productInfo.image = null;

    // isAvailable
    var isAvailable = document.querySelector('meta[name="availability"]');
    if (isAvailable && isAvailable.content == "true")
      productInfo.isAvailable = true;
    else productInfo.isAvailable = false;

    // price
    var backupPrice = null;
    var price = document.querySelector('meta[property="product:price:amount"]');
    if (json.offers["@type"] == "Offer") backupPrice = json.offers.price;
    price =
      (price && parseToInt(price.content)) ||
      (backupPrice && parseToInt(backupPrice));
    if (price && productInfo.isAvailable) productInfo.price = price;
    else productInfo.price = null;

    // discount
    var discount = document.querySelector(
      ".nk_add_to_cart_box .nk_discount_label"
    );
    if (discount && productInfo.isAvailable)
      productInfo.discount = parseToFloat(discount.innerText);
    else productInfo.discount = null;

    // expiration
    // i'm not sure you ment this kind of expiration :)... but for the sake fo challange...
    var expiration = document.querySelectorAll(".timer_nums .num");
    if (expiration[0]) {
      var s = parsToTimeStamp(expiration[0].innerHTML, "s");
      var m = parsToTimeStamp(expiration[1].innerHTML, "m");
      var h = parsToTimeStamp(expiration[2].innerHTML, "h");
      var d = parsToTimeStamp(expiration[3].innerHTML, "d");
      var currentTime = Math.floor(Date.now() / 1000);
      expiration = currentTime + s + m + h + d;
    } else expiration = 0;
    if (expiration) productInfo.expiration = expiration;
    else productInfo.expiration = null;

    // category
    var category = [];
    var tmpCategory = document.querySelectorAll(".woocommerce-breadcrumb li a");
    for (var i = 1; i < tmpCategory.length; i++) {
      category.push(tmpCategory[i].innerHTML);
    }
    if (category) productInfo.category = category;

    // brand
    var brand = null;
    if (json.brand) brand = json.brand.name;
    if (brand) productInfo.brand = brand;
    else productInfo.brand = null;

    // totalVotes
    var totalVotes = document.querySelector(
      ".main_title_and_rating .woocommerce-review-link"
    );
    if (totalVotes && productInfo.isAvailable)
      productInfo.totalVotes = parseToInt(totalVotes.innerHTML);
    else productInfo.totalVotes = null;

    // averageVote
    var averageVote = document.querySelector(
      '.main_title_and_rating strong[class="rating"]'
    );
    if (averageVote && productInfo.isAvailable)
      productInfo.averageVote = parseToFloat(averageVote.innerHTML);
    else productInfo.averageVote = null;

    return productInfo;
  }

  var sentFlag = false;
  function sendData() {
    try {
      var productInfo = extractProductData();

      if (productInfo && productInfo.sku && productInfo.title) {
        console.log(productInfo);
        sentFlag = true;
        // eslint-disable-next-line no-undef
        // yektanet.product('detail', productInfo);
      }
    } catch (e) {
      console.error(e);
      return false;
    }
  }

  var retry = 0;
  var intervalId = setInterval(function () {
    sendData();
    if (sentFlag == true || retry > 3) {
      clearInterval(intervalId);
    }
    retry++;
  }, 2000);
})();
