(function () {
  function parseToInt(e) {
    if (!e) {
      return;
    }

    e = e
      .toString()
      .replace(/۰/g, "0")
      .replace(/۱/g, "1")
      .replace(/۲/g, "2")
      .replace(/۳/g, "3")
      .replace(/۴/g, "4")
      .replace(/۵/g, "5")
      .replace(/۶/g, "6")
      .replace(/۷/g, "7")
      .replace(/۸/g, "8")
      .replace(/۹/g, "9")
      .replace(/\D/g, "");
    e = parseInt(e, 10);
    return e;
  }

  function parseToFloat(e) {
    e2 = parseToInt(e2);
    if (0 <= e2 && e2 <= 5) return e2;
    if (6 <= e2 && e2 <= 10) return e2 / 2;
    if (11 <= e2 && e2 <= 50) return e2 / 10;
    if (51 <= e2 && e2 <= 99) return e2 / 20;
    if (e2 >= 100) return e2 / 100;
    return e2;
  }

  function normalize(str) {
    str = str.toString().replace(/\t/g, "").replace(/\n/g, "");
    return str;
  }

  function productPage() {
    var pageType = document.querySelector("body");
    pageType = pageType && pageType.className.match("single-product");
    if (pageType) return true;
    return false;
  }

  function extractProductData() {
    if (!productPage()) {
      return;
    }

    var productInfo = {};

    var json = null;
    try {
      var ldJsons = document.querySelectorAll(
        'script[type="application/ld+json"]'
      );
      ldJsons.forEach(function (item) {
        item = JSON.parse(item.innerText);
        item = item["@graph"];
        for (var i = 0; i < item.length; i++) {
          if (item[i]["@type"] == "Product") json = item[i];
        }
      });
    } catch (error) {
      console.error(error);
    }

    // sku
    var sku = document.querySelector(".sku");
    var bakcupSku = json && json.sku;
    sku = (sku && sku.innerText) || bakcupSku;
    if (sku) productInfo.sku = sku;
    else productInfo.sku = null;

    // expiration
    // i'm not sure you ment this kind of expiration :)... but for the sake fo challange...
    var expiration = json.offers && json.offers[0].priceValidUntil;
    if (expiration) {
      expiration = expiration.split("-");
      expiration = expiration[1] + "/" + expiration[2] + "/" + expiration[0];
      expiration = new Date(expiration).getTime();
    }
    if (expiration) productInfo.expiration = expiration;
    else productInfo.expiration = null;

    //title
    var title = document.querySelector(".product_title");
    var backupTitle = json && json.name;
    title = (title && title.innerHTML) || backupTitle;
    if (title) productInfo.title = title;
    else productInfo.title = null;

    // image
    var image = document.querySelector(".woocommerce-product-gallery__image");
    var backupImage = json && json.image;
    image = (image && image.firstChild.href) || backupImage;
    if (image) productInfo.image = image;
    else productInfo.image = null;

    // price
    var price = document.querySelector(".price .woocommerce-Price-amount");
    price = price && parseToInt(price.textContent);
    if (price) productInfo.price = price;
    else productInfo.price = null;

    // isAvailable
    if (productInfo.price > 0) productInfo.isAvailable = true;
    else productInfo.isAvailable = false;

    // discount
    var discount = document.querySelector(".product-discount-value");
    discount = discount && parseToFloat(discount.textContent);
    if (discount) productInfo.discount = discount;
    else productInfo.discount = null;

    // category
    var category = [];
    var tmpCategory = document.querySelectorAll(".woocommerce-breadcrumb a");
    for (var i = 1; i < tmpCategory.length - 2; i++) {
      category.push(normalize(tmpCategory[i].textContent));
    }
    if (category) productInfo.category = category;

    // totalVotes
    var totalVotes =
      json && json.aggregateRating && json.aggregateRating.reviewCount;
    if (totalVotes) productInfo.totalVotes = parseToInt(totalVotes);
    else productInfo.totalVotes = null;

    // averageVote
    var averageVote =
      json && json.aggregateRating && json.aggregateRating.ratingValue;
    if (averageVote && productInfo.totalVotes)
      productInfo.averageVote = parseToFloat(averageVote);
    else productInfo.averageVote = null;

    return productInfo;
  }

  var sentFlag = false;
  function sendData() {
    try {
      var productInfo = extractProductData();

      if (productInfo && productInfo.title) {
        console.log(productInfo);
        sentFlag = true;
        // eslint-disable-next-line no-undef
        // yektanet.product('detail', productInfo);
      }
    } catch (e) {
      console.error(e);
      return false;
    }
  }

  var retry = 0;
  var intervalId = setInterval(function () {
    sendData();
    if (sentFlag == true || retry > 3) {
      clearInterval(intervalId);
    }
    retry++;
  }, 2000);
})();
