(function () {
  function parseToInt(e) {
    if (!e) {
      return;
    }

    e = e
      .toString()
      .replace(/۰/g, "0")
      .replace(/۱/g, "1")
      .replace(/۲/g, "2")
      .replace(/۳/g, "3")
      .replace(/۴/g, "4")
      .replace(/۵/g, "5")
      .replace(/۶/g, "6")
      .replace(/۷/g, "7")
      .replace(/۸/g, "8")
      .replace(/۹/g, "9")
      .replace(/\D/g, "");
    e = parseInt(e, 10);
    return e;
  }

  function parseToFloat(e) {
    e2 = parseToInt(e2);
    if (0 <= e2 && e2 <= 5) return e2;
    if (6 <= e2 && e2 <= 10) return e2 / 2;
    if (11 <= e2 && e2 <= 50) return e2 / 10;
    if (51 <= e2 && e2 <= 99) return e2 / 20;
    if (e2 >= 100) return e2 / 100;
    return e2;
  }

  function productPage() {
    var pageType = document.querySelector("body");
    pageType = pageType.className.match("single-product");
    if (pageType) return true;
    return false;
  }

  function extractProductData() {
    if (!productPage()) {
      return;
    }

    var productInfo = {};

    // sku
    var sku = document.querySelector('input[name="product_id"]');
    var backupSku = document.querySelector('button[name="add-to-cart"]');
    sku = (sku && sku.value) || (backupSku && backupSku.value);
    if (sku) productInfo.sku = sku;
    else productInfo.sku = null;

    //title
    var title = document.querySelector(".product_title h1");
    var backupTitle = document.querySelector(".woocommerce-breadcrumb");
    title =
      (title && title.firstChild.data) ||
      (backupTitle && backupTitle.lastChild.innerText);
    if (title) productInfo.title = title;
    else productInfo.title = null;

    // image
    var image = document.querySelector(".newkala_product_image img");
    var backupImage = document.querySelector(
      ".woocommerce-product-gallery__image img"
    );
    image = (image && image.src) || (backupImage && backupImage.src);
    if (image) productInfo.image = image;
    else productInfo.image = null;

    // price
    var price = document.querySelector(".woocommerce-Price-amount");
    price = price && parseToInt(price.innerText);
    if (price) productInfo.price = price;
    else productInfo.price = null;

    // isAvailable
    if (document.querySelector('meta[name="availability"]')) {
      var isAvailable = document.querySelector('meta[name="availability"]');
      if (isAvailable && isAvailable.content == "true")
        productInfo.isAvailable = true;
    } else productInfo.isAvailable = false;

    // discount
    // this website does'nt use discount
    // at least in this time...
    productInfo.discount = null;

    // expiration
    // this website does'nt use expiratoin
    // at least in this time...
    productInfo.expiration = null;

    // category
    var category = [];
    var tmpCategory = document.querySelectorAll(".woocommerce-breadcrumb li a");
    for (var i = 1; i < tmpCategory.length; i++) {
      category.push(tmpCategory[i].innerHTML);
    }
    if (category) productInfo.category = category;

    // brand
    var brand = document.querySelector(".seller_brand");
    if (brand) productInfo.brand = brand.innerHTML;
    else productInfo.brand = null;

    // totalVotes
    var totalVotes = document.querySelector(".woocommerce-review-link");
    if (totalVotes) productInfo.totalVotes = parseToInt(totalVotes.innerHTML);
    else productInfo.totalVotes = null;

    // averageVote
    var averageVote = document.querySelector('strong[class="rating"]');
    if (averageVote && productInfo.totalVotes)
      productInfo.averageVote = parseToFloat(averageVote.innerHTML);
    else productInfo.averageVote = null;

    return productInfo;
  }

  var sentFlag = false;
  function sendData() {
    try {
      var productInfo = extractProductData();

      if (productInfo && productInfo.sku && productInfo.title) {
        console.log(productInfo);
        sentFlag = true;
        // eslint-disable-next-line no-undef
        // yektanet.product('detail', productInfo);
      }
    } catch (e) {
      console.error(e);
      return false;
    }
  }

  var retry = 0;
  var intervalId = setInterval(function () {
    sendData();
    if (sentFlag == true || retry > 3) {
      clearInterval(intervalId);
    }
    retry++;
  }, 2000);
})();
