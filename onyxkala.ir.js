(function () {
  function parseToInt(e) {
    if (!e) {
      return;
    }

    e = e
      .toString()
      .replace(/۰/g, "0")
      .replace(/۱/g, "1")
      .replace(/۲/g, "2")
      .replace(/۳/g, "3")
      .replace(/۴/g, "4")
      .replace(/۵/g, "5")
      .replace(/۶/g, "6")
      .replace(/۷/g, "7")
      .replace(/۸/g, "8")
      .replace(/۹/g, "9")
      .replace(/\D/g, "");
    e = parseInt(e, 10);
    return e;
  }

  function parseToFloat(e2) {
    e2 = parseToInt(e2);
    if (0 <= e2 && e2 <= 5) return e2;
    if (6 <= e2 && e2 <= 10) return e2 / 2;
    if (11 <= e2 && e2 <= 50) return e2 / 10;
    if (51 <= e2 && e2 <= 99) return e2 / 20;
    if (e2 >= 100) return e2 / 100;
    return e2;
  }

  function calculateDiscount(price, oldPrice) {
    price = parseToInt(price);
    oldPrice = parseToInt(oldPrice);
    return (100 - (price * 100) / oldPrice);
  }

  function productPage() {
    var pageType = document.querySelector("body");
    pageType = pageType.className.match("single-product");
    if (pageType) return true;
    return false;
  }

  function extractProductData() {
    if (!productPage()) {
      return;
    }

    var productInfo = {};

    var json = null;
    try {
      var ldJsons = document.querySelectorAll(
        'script[type="application/ld+json"]'
      );
      ldJsons.forEach(function (item) {
        item = JSON.parse(item.innerText);
        item = item["@graph"];
        for (var i = 0; i < item.length; i++) {
          if (item[i]["@type"] == "Product") json = item[i];
        }
      });
    } catch (error) {
      console.error(error);
    }

    // sku
    var sku = document.querySelector(".sku_wrapper .sku");
    var backupSku = json.sku;
    sku = (sku && sku.innerHTML) || backupSku;
    if (sku) productInfo.sku = sku;
    else productInfo.sku = null;

    //title
    var title = document.querySelector(".info-header h1");
    var backupTitle = json.name;
    title =
      title && title.firstChild.data.toString().replace("\t\t\t\t\t\t", "");
    title = title || backupTitle;
    if (title) productInfo.title = title;
    else productInfo.title = null;

    // image
    var backupImage = null;
    var image = document.querySelector('meta[property="og:image"]');
    if (json) backupImage = json.image;
    image = (image && image.content) || backupImage;
    if (image) productInfo.image = image;
    else productInfo.image = null;

    // price
    var price = document.querySelector(
      ".product-info-box .matrix_wolffinal-price .woocommerce-Price-amount.amount"
    );
    var backupPrice = document.querySelector('meta[name="price"]');
    price =
      (price && parseToInt(price.innerText)) ||
      (backupPrice && parseToInt(backupPrice.content));
    if (price) productInfo.price = price;
    else productInfo.price = null;

    // isAvailable
    var isAvailable = document.querySelector(
      ".main-content .woocommerce-error"
    );
    isAvailable = isAvailable && isAvailable.innerText.match("موجود نیست");
    if (!isAvailable) productInfo.isAvailable = true;
    else productInfo.isAvailable = false;

    // discount
    var discount = document.querySelector(".product-info-box .dk-button-discount");
    var backupDiscount = document.querySelector(
      ".product-info-box .matrix_wolfold-price .woocommerce-Price-amount .amount"
    );
    if (productInfo.price && backupDiscount) {
      backupDiscount = calculateDiscount(
        productInfo.price,
        backupDiscount.innerText
      );
    }

    discount =
    discount && parseToFloat(discount.innerText) ||
      parseToFloat(backupDiscount);
    if (discount) productInfo.discount = discount;
    else productInfo.discount = null;

    // expiration
    // i'm not sure you ment this kind of expiration :)... but for the sake fo challange...
    var expiration = json.offers && json.offers[0].priceValidUntil;
    expiration = expiration.split("-");
    expiration = expiration[1] + "/" + expiration[2] + "/" + expiration[0];
    expiration = new Date(expiration).getTime();
    if (expiration) productInfo.expiration = expiration;
    else productInfo.expiration = null;

    // category
    var category = [];
    var tmpCategory = document.querySelectorAll(
      ".woocommerce-breadcrumb span a"
    );
    for (var i = 1; i < tmpCategory.length; i++) {
      category.push(tmpCategory[i].innerHTML);
    }
    if (category) productInfo.category = category;

    // brand
    var brand = json.brand;
    if (brand) productInfo.brand = json.brand;
    else productInfo.brand = null;

    // averageVote
    // i could'nt find a sample to work on...
    productInfo.averageVote = null;

    // totalVotes
    // i could'nt find a sample to work on...
    productInfo.totalVotes = null;

    return productInfo;
  }

  var sentFlag = false;
  function sendData() {
    try {
      var productInfo = extractProductData();

      if (productInfo && productInfo.sku && productInfo.title) {
        console.log(productInfo);
        sentFlag = true;
        // eslint-disable-next-line no-undef
        // yektanet.product('detail', productInfo);
      }
    } catch (e) {
      console.error(e);
      return false;
    }
  }

  var retry = 0;
  var intervalId = setInterval(function () {
    sendData();
    if (sentFlag == true || retry > 3) {
      clearInterval(intervalId);
    }
    retry++;
  }, 2000);
})();
