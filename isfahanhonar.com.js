(function () {
    function parseToInt(e) {
      if (!e) {
        return;
      }
  
      e = e
        .toString()
        .replace(/۰/g, "0")
        .replace(/۱/g, "1")
        .replace(/۲/g, "2")
        .replace(/۳/g, "3")
        .replace(/۴/g, "4")
        .replace(/۵/g, "5")
        .replace(/۶/g, "6")
        .replace(/۷/g, "7")
        .replace(/۸/g, "8")
        .replace(/۹/g, "9")
        .replace(/\D/g, "");
      e = parseInt(e, 10);
      return e;
    }

    function calculateDiscount(price, oldPrice) {
        price = parseToInt(price);
        oldPrice = parseToInt(oldPrice);
        return (100 - (price * 100) / oldPrice);
    }
  
    function productPage() {
      var pageType = document.querySelector('body');
      if (pageType.id == "product") return true;
      return false;
    }
  
    function extractProductData() {
      if (!productPage()) {
        return;
      }
  
      var productInfo = {};
  
      // sku
      var sku = document.querySelector('span[itemprop="sku"]');
      var backupSku = document.querySelector('input[name="id_product"]');
      sku = (sku && sku.innerHTML) || (backupSku && backupSku.value);
      if (backupSku) productInfo.sku = sku;
      else productInfo.sku = null;
  
      //title
      var title = document.querySelector('h1[itemprop="name"]');
      var backupTitle = document.querySelector(".navigation-pipe");
      backupTitle = backupTitle && backupTitle.nextSibling.nextSibling.innerText.split('>');
      title = title && title.innerText || backupTitle && backupTitle[backupTitle.length - 1];
      if (title) productInfo.title = title;
      else productInfo.title = null;
  
      // image
      var image = document.getElementById('bigpic');
      image = image && image.src;
      if (image) productInfo.image = image;
      else productInfo.image = null;
  
      // price
      var price = document.querySelector('.pb-right-column #our_price_display');
      price = price && parseToInt(price.innerHTML);
      if (price) productInfo.price = price;
      else productInfo.price = null;
  
      // isAvailable
      var isAvailable = document.getElementById("availability_value");
      if (isAvailable && isAvailable.innerHTML !== "این محصول در انبار موجود نیست"){
          productInfo.isAvailable = true;
      }
      else productInfo.isAvailable = false;
  
      // category
      var category = [];
      var tmpCategory = document.querySelectorAll(".navigation_page span a");
      for(var i = 0; i < tmpCategory.length; i++){
          category.push(tmpCategory[i].title);
      }
      if (category) productInfo.category = category;
      else productInfo.category = null;
  
      // brand
      var brand = document.querySelector('meta[property="og:site_name"]');
      brand = brand && brand.content;
      if (brand) productInfo.brand = brand;
      else productInfo.brand = null;

    // discount
    var discount = document.querySelector(
      "#old_price_display .price"
    );
    if (productInfo.price && discount) {
      discount = calculateDiscount(
        productInfo.price,
        discount.innerText
      );
    }
    discount = discount;
    if (discount) productInfo.discount = discount;
    else productInfo.discount = null;
  
    // expiration
    // this website does'nt use expiratoin
    // at least in this time...
    productInfo.expiration = null;

    // averageVote
    var countStars = 0;
    var averageVote = document.querySelectorAll("#product_comments_block_extra .star_content .star");
    for(var i2 = 0; i2 < averageVote.length; i2++){
        if(averageVote[i2].className.match("star_on")) countStars++;
    }
    if(countStars) productInfo.averageVote = countStars;
    else productInfo.averageVote = null;

    // totalVotes
    var totalVotes = document.querySelector(".reviews span");
    if(totalVotes) productInfo.totalVotes = parseToInt(totalVotes.innerHTML);
    else productInfo.totalVotes = null

      return productInfo;
    }
  
    var sentFlag = false;
    function sendData() {
      try {
        var productInfo = extractProductData();
  
        if (productInfo && productInfo.sku && productInfo.title) {
          console.log(productInfo);
          sentFlag = true;
          // eslint-disable-next-line no-undef
          // yektanet.product('detail', productInfo);
        }
      } catch (e) {
        console.error(e);
        return false;
      }
    }
  
    var retry = 0;
    var intervalId = setInterval(function () {
      sendData();
      if (sentFlag == true || retry > 3) {
        clearInterval(intervalId);
      }
      retry++;
    }, 2000);
  })();
  